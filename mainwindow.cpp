#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "mmgauge.h"
#include <QtNetwork>
#include <QMessageBox>
#include <QFileDialog>
#include <QtDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{

    ui->setupUi(this);

    //For debugging
//    qDebug("%s size: %d", qPrintable(timer.objectName()), sizeof(timer));
//    qDebug() << sizeof(btnSave) << "\n";

    //Start initializing gauges and charts.
    gaugeLeft.setAngleStart(210);
    gaugeLeft.setAngleEnd(330);
    gaugeLeft.setNumStart(0);
    gaugeLeft.setNumEnd(5000);                //Set default max range range.
    gaugeLeft.setNumCurrent(0);
    gaugeLeft.setNumScaleMark(6);           //5 sections in full scale range.
    gaugeLeft.setPoleScale(10);
    gaugeLeft.setTitle(tr("TX Speed(Kbps)"));
    gaugeLeft.setOffsetTitleTextX(0.4);
    gaugeLeft.setOffsetTitleTextY(0.6);

    gaugeRight.setAngleStart(210);
    gaugeRight.setAngleEnd(330);
    gaugeRight.setNumStart(-100);
    gaugeRight.setNumEnd(0);
    gaugeRight.setNumCurrent(-100);
    gaugeRight.setPoleScale(10);
    gaugeRight.setTitle(tr("RSSI(dBm)"));
    gaugeRight.setOffsetTitleTextX(0.3);
    gaugeRight.setOffsetTitleTextY(0.6);

    mmScene.addItem(&gaugeLeft);
    mmScene.addItem(&gaugeRight);

    chartThroughput.addSeries(&serTxThroughput);      //Attache data to chart
    chartThroughput.addSeries(&serRxThroughput);      //Attache data to chart
    chartRSSI.addSeries(&serRSSI);                  //Attache data to chart

    axisXThroughput.setRange(0, 100);
    axisXThroughput.setLabelFormat("%d");
    chartThroughput.addAxis(&axisXThroughput, Qt::AlignBottom);
    serTxThroughput.attachAxis(&axisXThroughput);
    serRxThroughput.attachAxis(&axisXThroughput);
    axisYThroughput.setRange(0, 5000);
    chartThroughput.addAxis(&axisYThroughput, Qt::AlignLeft);
    serTxThroughput.attachAxis(&axisYThroughput);
    serRxThroughput.attachAxis(&axisYThroughput);

    //In default, set serTxThroughput as visible.
    serTxThroughput.setVisible(true);
    serRxThroughput.setVisible(false);

    axisXRSSI.setRange(0, 100);
    axisXRSSI.setLabelFormat("%d");
    chartRSSI.addAxis(&axisXRSSI, Qt::AlignBottom);
    serRSSI.attachAxis(&axisXRSSI);
    axisYRSSI.setRange(-100, 0);
    chartRSSI.addAxis(&axisYRSSI, Qt::AlignLeft);
    serRSSI.attachAxis(&axisYRSSI);

    chartThroughput.setTitle(tr("Throughput"));
    chartRSSI.setTitle(tr("RSSI"));

    //Hide legends to save some space.
    chartThroughput.legend()->hide();
    chartRSSI.legend()->hide();

    chartThroughput.setAnimationOptions(QChart::SeriesAnimations);
    chartRSSI.setAnimationOptions(QChart::SeriesAnimations);

    viewThroughput.setChart(&chartThroughput);
    viewRSSI.setChart(&chartRSSI);
    viewThroughput.setRenderHint(QPainter::Antialiasing);
    viewRSSI.setRenderHint(QPainter::Antialiasing);

    ui->gridLayout->addWidget(&viewThroughput, 1, 0, 1, 3);     //Add chart view to UI.
    ui->gridLayout->addWidget(&viewRSSI, 1, 3, 1, 3);           //Add chart view to UI.

    //Re-place the UI components which already placed in .ui files.
    ui->gridLayout->addWidget(ui->label, 2, 0, 1, 1);
    ui->gridLayout->addWidget(ui->editHostName, 2, 1, 1, 1);
    ui->gridLayout->addWidget(ui->label_2, 2, 2, 1, 1);
    ui->gridLayout->addWidget(ui->editHostPort, 2, 3, 1, 1);
    ui->gridLayout->addWidget(ui->btnConnect, 2, 4, 1, 1);
    ui->gridLayout->addWidget(ui->btnQuit, 2, 5, 1, 1);
    ui->btnQuit->setEnabled(false);

    ui->gridLayout->setRowStretch(0,3);
    ui->gridLayout->setRowStretch(1,2);
    ui->gridLayout->setRowStretch(2,0);

    //Create sub-grid-layout and widgets for options.
    ui->gridLayout->addLayout(&layoutConfig, 0, 6, -1, 3);      //Add a sub grid layout inside main grid layout for options.
    layoutConfig.addWidget(&grpGaugeConfig, 0, 0, 1, 1);       //Add group box for gauges.
    layoutConfig.addWidget(&grpChartConfig, 1, 0, 1, 1);        //Add group box for charts.
    layoutConfig.addWidget(&grpUtil, 2, 0, 1, 1);               //Add group box for other options.
    layoutConfig.addWidget(&grpAbout, 3, 0, 1, 1);               //Add a button to place logo.

    grpGaugeConfig.setLayout(&layoutGaugeConfig);
    grpChartConfig.setLayout(&layoutChartConfig);
    grpUtil.setLayout(&layoutUtil);
    grpAbout.setLayout(&layoutAbout);

    grpGaugeConfig.setTitle(tr("Gauge Settings"));
    grpChartConfig.setTitle(tr("Chart Settings"));
    grpUtil.setTitle(tr("Utilities"));
    grpAbout.setTitle(tr("Credit"));

    //Add options for gauges
    lblGauge.setText(tr("Left Gauge:"));
    layoutGaugeConfig.addWidget(&lblGauge, 0, 0, 1, 1, Qt::AlignLeft);
    cbbGaugeSelect.addItem(tr("Upstream"));
    cbbGaugeSelect.addItem(tr("Downstream"));
    cbbGaugeSelect.setToolTip(tr("Choose downstream/upstream to be shown on left gauge"));
    layoutGaugeConfig.addWidget(&cbbGaugeSelect, 0, 1, 1, 1, Qt::AlignHCenter);
    lblGauge2.setText(tr("Left Gauge MAX:"));
    layoutGaugeConfig.addWidget(&lblGauge2, 1, 0, 1, 1, Qt::AlignLeft);
    editLeftGaugeMax.setText(tr("%1").arg(gaugeLeft.getNumEnd()));
    editLeftGaugeMax.setToolTip(tr("Set the max value of left gauge, in Kbps unit"));
    layoutGaugeConfig.addWidget(&editLeftGaugeMax, 1, 1, 1, 1, Qt::AlignHCenter);
    chkSyncWithChart.setCheckState(Qt::Checked);
    chkSyncWithChart.setText(tr("Sync to chart"));
    chkSyncWithChart.setToolTip(tr("The value of thoughput gauge max will also be applied to throughput chart"));
    layoutGaugeConfig.addWidget(&chkSyncWithChart, 2, 1, 1, 1, Qt::AlignHCenter);

    //Add options for utils
    btnSave.setText(tr("Save log"));
    btnSave.setToolTip(tr("Save the logged data to file"));
    layoutUtil.addWidget(&btnSave, 0, 0, 1, 1, Qt::AlignTop|Qt::AlignHCenter);
    btnLoad.setText(tr("Load log"));
    btnLoad.setToolTip(tr("Load the logged data from file"));
    layoutUtil.addWidget(&btnLoad, 0, 1, 1, 1, Qt::AlignTop|Qt::AlignHCenter);
    btnClear.setText(tr("Clear log"));
    btnClear.setToolTip(tr("Clear current logged data"));
    layoutUtil.addWidget(&btnClear, 1, 0, 1, 1, Qt::AlignTop|Qt::AlignHCenter);

    //Add logo
    picLogo.load(":/MM_Logo_Full_Shrinked.png");
    lblLogo.setPixmap(picLogo);
    lblLogo.setScaledContents(true);
    layoutAbout.addWidget(&lblLogo, 0, 0, 1, 1, Qt::AlignVCenter|Qt::AlignHCenter);
    grpAbout.setStyleSheet ("background-color: rgb(255, 255, 255);");

    //Handle events of option changes.
    connect(&cbbGaugeSelect, SIGNAL(currentIndexChanged(int)), this, SLOT(cfgLeftGaugeTypeChange(int)));
    connect(&editLeftGaugeMax, SIGNAL(editingFinished()), this, SLOT(cfgLeftGaugeMaxChange()));
    connect(&btnSave, SIGNAL(clicked()), this, SLOT(btnSaveClicked()));
    connect(&btnLoad, SIGNAL(clicked()), this, SLOT(btnLoadClicked()));
    connect(&btnClear, SIGNAL(clicked()), this, SLOT(btnClearClicked()));

    ui->DashboardView->setScene(&mmScene);
    ui->DashboardView->setRenderHint(QPainter::Antialiasing);
    ui->DashboardView->show();

    connect(&dataPump, SIGNAL(newData(QString)), this, SLOT(handleIncomingData(QString)));
    connect(&dataPump, SIGNAL(error(int, QString)), this, SLOT(handleError(int, QString)));
//    QObject::connect(&dataPump, SIGNAL(debug(int, QString)), this, SLOT(handleDebug(int, QString)));

    //Init animation for gauge hands.
    aniGaugeLeft.setTargetObject(&gaugeLeft);
    aniGaugeLeft.setPropertyName("numCurrent");
    aniGaugeLeft.setDuration(300);                  //Set animation duration to 300ms.
    aniGaugeLeft.setEasingCurve(QEasingCurve::InOutBack);        //Set easing curve.
    aniGaugeRight.setTargetObject(&gaugeRight);
    aniGaugeRight.setPropertyName("numCurrent");
    aniGaugeRight.setDuration(300);                  //Set animation duration to 300ms.
    aniGaugeRight.setEasingCurve(QEasingCurve::InOutBack);        //Set easing curve.

    //Setup response to enter-pressed on host/port editor.
    connect(ui->editHostName, SIGNAL(returnPressed()), this, SLOT(on_btnConnect_clicked()));
    connect(ui->editHostPort, SIGNAL(returnPressed()), this, SLOT(on_btnConnect_clicked()));

    //Start timer to refresh gauges.
    connect(&timer, SIGNAL(timeout()), this, SLOT(refresh()));
    timer.start(1000 / 33);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::refresh()
{
    int width = ui->DashboardView->width();
    int height = ui->DashboardView->height();

    //Set the size of 2 gauges.
    gaugeLeft.setPos(QPoint(-width/4, -(height/2-width/4)));
    gaugeLeft.setRect(QRect(-width/4, -width/4, width/2, width/2));
    gaugeRight.setPos(QPoint(width/4, -(height/2-width/4)));
    gaugeRight.setRect(QRect(-width/4, -width/4, width/2, width/2));

    ui->DashboardView->centerOn(QPoint(0,0));         //Force the view centered to (0,0) of scene.
    ui->DashboardView->setSceneRect(QRect(-width/2-100, -height/2-100, width+100, height+100));         //The view area of scene will be fixed and centered, not moving.

//    mmScene.advance();
    ui->DashboardView->viewport()->update();
}

void MainWindow::handleIncomingData(QString data)
{
    float rateTemp;                     //Temporarily store the data rate.

    QString inData=data;                //Save the copy of incoming data.

    inData.append(tr("%1\n").arg(QDateTime::currentMSecsSinceEpoch())); //Add local time stamp to end of record.

    if(dataList.size()>=dataMaxSize){
        dataList.removeFirst();             //If size reached buffer limitation, remove first record.
    }
    dataList.append(inData);                  //Store data record to buffer.

    switch(cbbGaugeSelect.currentIndex()){      //Pick the TX or RX rate data according to option setting.
    case 0:
        rateTemp=parseTxThroughput(inData);
        break;
    case 1:
        rateTemp=parseRxThroughput(inData);
        break;
    default:
        rateTemp=parseTxThroughput(inData);
        break;
    }

//    gaugeLeft.setNumCurrent(rateTemp);
    //Kick-off animation of left gauge hand.
    aniGaugeLeft.stop();
    aniGaugeLeft.setStartValue(gaugeLeft.getNumCurrent());
    aniGaugeLeft.setEndValue(rateTemp);
    aniGaugeLeft.start();

//    gaugeRight.setNumCurrent(parseRSSI(data));
    //Kick-off animation of right gauge hand.
    aniGaugeRight.stop();
    aniGaugeRight.setStartValue(gaugeRight.getNumCurrent());
    aniGaugeRight.setEndValue(parseRSSI(inData));
    aniGaugeRight.start();

    if(serTxThroughput.count()>=dataMaxSize){
        serTxThroughput.removePoints(0,1);    //If size reached limitation, remove first one.
    }
    serTxThroughput.append(count, parseTxThroughput(inData));  //Append the new rate data to chart.

    if(serRxThroughput.count()>=dataMaxSize){
        serRxThroughput.removePoints(0,1);    //If size reached limitation, remove first one.
    }
    serRxThroughput.append(count, parseRxThroughput(inData));  //Append the new rate data to chart.

    if(serRSSI.count()>=dataMaxSize){
        serRSSI.removePoints(0,1);          //If size reached limitation, remove first one.
    }
    serRSSI.append(count, parseRSSI(inData));

    if(count>100){                          //If more than 100 records received, adjust the x-axis of charts to display the latest 100 data.
        axisXThroughput.setRange(count-100, count);
        axisXRSSI.setRange(count-100, count);
    }

    ui->statusbar->showMessage(tr("Total %1 record received").arg(dataList.size()));

    count++;
}

void MainWindow::handleError(int socketError, const QString message)
{
    switch (socketError) {
    case QAbstractSocket::HostNotFoundError:
        QMessageBox::information(this, tr("MM Dashboard"),
                                 tr("The host was not found. Please check the "
                                    "host and port settings."));
        break;
    case QAbstractSocket::ConnectionRefusedError:
        QMessageBox::information(this, tr("MM Dashboard"),
                                 tr("The connection was refused by the peer. "
                                    "Make sure the data server is running, "
                                    "and check that the host name and port "
                                    "settings are correct."));
        break;
    default:
        QMessageBox::information(this, tr("MM Dashboard"),
                                 tr("The following error occurred: %1.")
                                 .arg(message));
    }

    dataPump.endConnect();              //If any error found, just disconnect.
    ui->btnConnect->setEnabled(true);
    ui->btnQuit->setEnabled(false);
    btnSave.setEnabled(true);              //Enable save button
    btnLoad.setEnabled(true);              //Enable load button
}

void MainWindow::handleDebug(int socketError, const QString message)
{
    ui->statusbar->showMessage(tr("%1, %2").arg(socketError).arg(message));
}

void MainWindow::cfgLeftGaugeTypeChange(int index)
{
    switch(index){
    case 0:
        gaugeLeft.setTitle(tr("TX Speed(Kbps)"));
        serTxThroughput.setVisible(true);
        serRxThroughput.setVisible(false);
        break;
    case 1:
        gaugeLeft.setTitle(tr("RX Speed(Kbps)"));
        serTxThroughput.setVisible(false);
        serRxThroughput.setVisible(true);
        break;
    default:
        ;
    }
}

void MainWindow::cfgLeftGaugeMaxChange()
{
    gaugeLeft.setNumEnd(editLeftGaugeMax.text().toFloat());
    if(chkSyncWithChart.checkState()==Qt::Checked){
        axisYThroughput.setRange(0, editLeftGaugeMax.text().toFloat());
    }
}

void MainWindow::btnSaveClicked()
{
    if(dataList.isEmpty()){
        QMessageBox::information(this, tr("MM Dashboard"),
                                 tr("Record buffer is empty, "
                                    "nothing to save."));
        return;             //Return if no data to save.
    }

    QFile file;
    QString fileName;
    QTextStream fileStream(&file);          //Create a text stream for file R/W.

    fileName=QFileDialog::getSaveFileName(this, tr("Save log file"),\
                                          "", tr("Log files (*.mlg)"));   //Get file name.

    if(fileName.isEmpty())
        return;             //If no file selected or error found, just do nothing.

    file.setFileName(fileName);
    if (!file.open(QIODevice::ReadWrite | QIODevice::Text)){
        QMessageBox::information(this, tr("MM Dashboard"),
                                 tr("Cannot open file!"));
        return;             //If failed to open file, quit.
    }

    fileStream << tr("Morse Micro mmnlspy dashboard log file V1.0\n");
    for(int i=0;i<dataList.size();i++){
        fileStream << dataList.at(i);
    }

    file.close();

    ui->statusbar->showMessage(tr("Logged data %1 lines successfully saved.").arg(dataList.size()));
}

void MainWindow::btnLoadClicked()
{
    if(!dataList.isEmpty()){
        if(QMessageBox::warning(this, tr("MM Dashboard"),
                                 tr("Current data buffer is not empty."
                                    "Do you want to discard current data?"), \
                                QMessageBox::Ok, QMessageBox::Cancel)==QMessageBox::Cancel)
            return;             //Return if cancel loading file.
    }

    QFile file;
    QString fileName;
    QTextStream fileStream(&file);          //Create a text stream for file R/W.

    fileName=QFileDialog::getOpenFileName(this, tr("Load log file"),\
                                          "", tr("Log files (*.mlg)"));   //Get file name.

    if(fileName.isEmpty())
        return;             //If no file selected or error found, just do nothing.

    file.setFileName(fileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        QMessageBox::information(this, tr("MM Dashboard"),
                                 tr("Cannot open file!"));
        return;             //If failed to open file, quit.
    }

//  Note: due to bug QTBUG-24367, the ::canReadLine() always return false, so below codes are commented.

//    if (!file.canReadLine()){
//        QMessageBox::information(this, tr("MM Dashboard"),
//                                 tr("Cannot read record lines!"));
//        file.close();
//        return;             //If no data lines found (end with '\n'), quit.
//    }

    fileStream.readLine();      //Skip first comment line.

    dataList.clear();           //Clear current buffer.
    while(!fileStream.atEnd()){
        dataList.append(fileStream.readLine());     //Read file line by line.
    }

    file.close();

    //Start to update charts with new loaded data.
    serTxThroughput.clear();
    serRxThroughput.clear();
    serRSSI.clear();
    for(int i=0;i<dataList.size();i++){
        serTxThroughput.append(i, parseTxThroughput(dataList.at(i)));
        serRxThroughput.append(i, parseRxThroughput(dataList.at(i)));
        serRSSI.append(i, parseRSSI(dataList.at(i)));
    }

    count=dataList.size();          //Adjust the pointer.

    ui->statusbar->showMessage(tr("Total %1 lines data successfully loaded.").arg(dataList.size()));
}

void MainWindow::btnClearClicked()
{
    dataList.clear();           //Clear current buffer.
    serTxThroughput.clear();
    serRxThroughput.clear();
    serRSSI.clear();
    count=0;

    aniGaugeLeft.stop();
    aniGaugeLeft.setStartValue(gaugeLeft.getNumCurrent());
    aniGaugeLeft.setEndValue(gaugeLeft.getNumStart());
    aniGaugeLeft.start();

    aniGaugeRight.stop();
    aniGaugeRight.setStartValue(gaugeRight.getNumCurrent());
    aniGaugeRight.setEndValue(gaugeRight.getNumStart());
    aniGaugeRight.start();

    ui->statusbar->showMessage(tr("Current data buffer cleared."));
}

void MainWindow::on_btnConnect_clicked()
{
    //The enter-key-pressed handler will also be connected to this slot, so need to check if btnConnect is enabled or not.
    if(ui->btnConnect->isEnabled()==true){
        ui->btnConnect->setEnabled(false);
        dataPump.connectTo(ui->editHostName->text(), ui->editHostPort->text().toUInt());
        ui->btnQuit->setEnabled(true);
        btnSave.setEnabled(false);              //Disable save button
        btnLoad.setEnabled(false);              //Disable load button
    }
}

void MainWindow::on_btnQuit_clicked()
{
    ui->btnQuit->setEnabled(false);
    dataPump.endConnect();
    ui->btnConnect->setEnabled(true);
    btnSave.setEnabled(true);              //Enable save button
    btnLoad.setEnabled(true);              //Enable load button
}

//**************************************
// Parse the throughput number from string received from mmnlspy.
//**************************************
float MainWindow::parseTxThroughput(const QString &data)
{
    return data.section(',', 9, 9).toFloat();
}

float MainWindow::parseRxThroughput(const QString &data)
{
    return data.section(',', 8, 8).toFloat();
}

//**************************************
// Parse the throughput number from string received from mmnlspy.
//**************************************
int MainWindow::parseRSSI(const QString &data)
{
    return data.section(',', 7, 7).toInt();
}

