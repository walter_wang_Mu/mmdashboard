#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include "mmgauge.h"
#include "mmconnection.h"
#include "mmchartview.h"
#include <QGridLayout>
#include <QPushButton>
#include <QComboBox>
#include <QGroupBox>
#include <QLineEdit>
#include <QLabel>
#include <QCheckBox>
#include <QPropertyAnimation>
#include <QTimer>
#include <QtCharts/QChart>
#include <QtCharts/QLineSeries>
#include <QtCharts/QValueAxis>
#include <QtCharts/QLegend>
#include <QtCharts/QChartView>
#include <QChart>
using namespace QtCharts;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QTimer timer;
    QGraphicsScene mmScene;                     //Instantiate the scene which will contains 2 gauges.

    mmGauge gaugeLeft, gaugeRight;              //Instantiate 2 gauges.
    QChart chartThroughput, chartRSSI;          //Instantiate 2 charts.
    mmChartView viewThroughput, viewRSSI;        //Instantiate 2 chart views.
    QLineSeries serTxThroughput, serRxThroughput, serRSSI;         //3 data series as data shource of charts.
    QValueAxis axisXThroughput, axisYThroughput, axisXRSSI, axisYRSSI;
    QStringList dataList;                       //Store all the data came from mmnlspy.

    int dataMaxSize=65536;                      //Set the maximum size of dataList, once reached this size, the earliest record will be removed (FIFO).

    mmConnection dataPump;                      //Maintain data connection and format handling, then feed data to GUI.

    //Note: It is ugly to define/add ui components outside ui designer scope.
    //      But have to do this to avoid re-struct the original ui.
    QGridLayout layoutConfig;                  //This grid layout will be appended to main window for config options.

    QGroupBox grpGaugeConfig, grpChartConfig, grpUtil, grpAbout;      //GroupBoxes for different config groups.
    QGridLayout layoutGaugeConfig, layoutChartConfig, layoutUtil, layoutAbout;   //Define layouts for each groupbox.

    //Define options for gauges.
    QComboBox cbbGaugeSelect;
    QLabel lblGauge, lblGauge2;
    QLineEdit editLeftGaugeMax;
    QCheckBox chkSyncWithChart;

    //Define options for charts

    //Define options for utils
    QPushButton btnSave, btnLoad, btnClear;

    //Define logo
    QLabel lblLogo;
    QPixmap picLogo;

    //Define animation controller.
    QPropertyAnimation aniGaugeLeft, aniGaugeRight;

public slots:
    void refresh();
    void handleIncomingData(QString data);
    void handleError(int socketError, const QString message);
    void handleDebug(int socketError, const QString message);
    void cfgLeftGaugeTypeChange(int index);             //Response to the signal of ComboBox changes.
    void cfgLeftGaugeMaxChange();                       //Response to the signal of left gauge max changes.
    void btnSaveClicked();
    void btnLoadClicked();
    void btnClearClicked();

private slots:
    void on_btnConnect_clicked();
    void on_btnQuit_clicked();

private:
    Ui::MainWindow *ui;

    ulong count=0;                //To store the count of record received.

    float parseTxThroughput(const QString &data);
    float parseRxThroughput(const QString &data);
    int parseRSSI(const QString &data);
};
#endif // MAINWINDOW_H
