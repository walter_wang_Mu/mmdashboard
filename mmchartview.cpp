#include "mmchartview.h"

mmChartView::mmChartView()
{

}

void mmChartView::mouseMoveEvent(QMouseEvent *pEvent)
{
    if (mmLeftPressed)
    {
        QPoint temp = pEvent->pos() - mmPos;
        this->chart()->scroll(-temp.x(), 0);
        mmPos = pEvent->pos();
    }
}

void mmChartView::mousePressEvent(QMouseEvent *pEvent)
{
    if (pEvent->button() == Qt::LeftButton)
    {
        mmLeftPressed = true;
        mmPos = pEvent->pos();
        this->setCursor(Qt::OpenHandCursor);
    }
}

void mmChartView::mouseReleaseEvent(QMouseEvent *pEvent)
{
    if (pEvent->button() == Qt::LeftButton)
    {
        mmLeftPressed = false;
        this->setCursor(Qt::ArrowCursor);
    }
}
