#ifndef MMCHARTVIEW_H
#define MMCHARTVIEW_H

#include <QObject>
#include <QtCharts/QChartView>
#include <QChart>
using namespace QtCharts;

class mmChartView : public QChartView
{
    Q_OBJECT
public:
    mmChartView();

protected:
    virtual void mouseMoveEvent(QMouseEvent *pEvent) override;
    virtual void mousePressEvent(QMouseEvent *pEvent) override;
    virtual void mouseReleaseEvent(QMouseEvent *pEvent) override;

private:
    bool mmLeftPressed=false;
    QPoint mmPos;
};

#endif // MMCHARTVIEW_H
