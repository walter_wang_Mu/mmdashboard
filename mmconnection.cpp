#include "mmconnection.h"
#include <QtNetwork>

mmConnection::mmConnection()
{

}

mmConnection::~mmConnection()
{
    if(isRunning()){
        mutex.lock();
        quit = true;
        mutex.unlock();
        wait();
    }
}

void mmConnection::connectTo(const QString &host, quint16 port)
{
    mutex.lock();
    this->host = host;
    this->port = port;

    quit=false;
    if (!isRunning())
        start();

    mutex.unlock();
}

void mmConnection::endConnect()
{
    mutex.lock();
    quit = true;
    mutex.unlock();
    wait();
}

void mmConnection::run()
{
    bool quitFlag=false;

    mutex.lock();
    QString serverName = host;
    quint16 serverPort = port;
    quitFlag = quit;
    mutex.unlock();

    QTcpSocket socket;
    const int Timeout = 5 * 1000;
    QString receivedBuf, receivedData;
    int index;

    while (!quitFlag) {
        if(socket.state()!=QAbstractSocket::ConnectedState){
            socket.connectToHost(serverName, serverPort);

            if (!socket.waitForConnected(Timeout)) {
                emit error(socket.error(), socket.errorString());
                return;
            }
        }

        if(socket.waitForReadyRead(1000)){
            if(socket.bytesAvailable()!=0){
                receivedBuf.append(socket.readAll());
            }

            if(receivedBuf.contains(recDelimiter)){             //A full mmnlspy record ends with delimiter.
                receivedData=receivedBuf.section(recDelimiter, 0, 0);   //Get first record.
                emit newData(receivedData);             //Send a new record to GUI.
                index=receivedBuf.indexOf(recDelimiter);        //Check the 1st position of delimiter.
                if(index!=-1){                          //Check if delimiter exists.
                    receivedBuf=receivedBuf.right(receivedBuf.length()-index-1);    //Cut off the 1st record ends with delimiter.
                }
            }
        }

        emit debug(socket.error(), socket.errorString());

        msleep(10);

        mutex.lock();
        quitFlag = quit;
        mutex.unlock();
    }

    socket.disconnectFromHost();
    while(socket.state()!=QAbstractSocket::UnconnectedState);
//    socket.waitForDisconnected();
}
