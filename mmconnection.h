#ifndef MMCONNECTION_H
#define MMCONNECTION_H

#include <QObject>
#include <QWidget>
#include <QThread>
#include <QMutex>

class mmConnection : public QThread
{
    Q_OBJECT

public:
    mmConnection();
    ~mmConnection();

    void connectTo(const QString &hostName, quint16 port);
    void endConnect();
    void run() override;

signals:
    void newData(const QString data);
    void error(int socketError, const QString message);
    void debug(int socketError, const QString message);

private:
    QString host;
    quint16 port;
    QMutex mutex;
    bool quit=false;
    QChar recDelimiter=QChar('\n');              //Define the delimiter of records.
};

#endif // MMCONNECTION_H
