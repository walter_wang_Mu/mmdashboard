#include "mmgauge.h"

#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QPainter>
#include <QRandomGenerator>
#include <QtMath>

mmGauge::mmGauge()
{

}

QRectF mmGauge::boundingRect() const
{
    return bRect;
}

QPainterPath mmGauge::shape() const
{
    QPainterPath path;
    path.addRect(bRect);
    return path;
}

void mmGauge::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{

//    painter->drawRect(bRect);       //For reference purpose only during design phase.

    QPen pen;  //Creates a default pen

    //Set pen style to draw cirlces & arcs.
    pen.setStyle(Qt::SolidLine);
    pen.setWidth(2);
    pen.setBrush(Qt::black);
    pen.setCapStyle(Qt::RoundCap);
    pen.setJoinStyle(Qt::RoundJoin);
    painter->setPen(pen);
    painter->drawEllipse(bRect.adjusted(offsetScale, offsetScale, -offsetScale, -offsetScale));     //Draw outer circle.
    pen.setBrush(Qt::blue);
    painter->setPen(pen);
    int angleTemp;
    if(angleEnd > angleStart)
    {
        angleTemp=-(angleEnd-angleStart-360);
    }else{
        angleTemp=angleStart-angleEnd;
    }
    painter->drawArc(bRect.adjusted(offsetScale+10, offsetScale+10, -offsetScale-10, -offsetScale-10), \
                     angleEnd*16, angleTemp*16);        //Draw outer arc of scale.


    //Set pen style to draw scales.
    pen.setStyle(Qt::SolidLine);
    pen.setWidth(2);
    pen.setBrush(Qt::blue);
    pen.setCapStyle(Qt::RoundCap);
    pen.setJoinStyle(Qt::RoundJoin);
    painter->setPen(pen);
    for(int i=0;i<numScaleMark;i++)
    {
        painter->save();
        int angleTemp=angleStart-angleEnd;
        if(angleTemp<0)angleTemp+=360;
        painter->rotate(360-angleStart+angleTemp*i/(numScaleMark-1));
        painter->drawLine(QPoint(bRect.width()/2-offsetScale-10, 0), \
                          QPoint(bRect.width()/2-offsetScale-20, 0));
        painter->restore();
    }

    //Set pen style to draw text of scales.
    pen.setStyle(Qt::SolidLine);
    pen.setWidth(1);
    pen.setBrush(Qt::blue);
    painter->setPen(pen);
    QFont font = painter->font();
    font.setPixelSize(18);
    painter->setFont(font);
    for(int i=0;i<numScaleMark;i++)
    {
        painter->save();
        int angleTemp=angleStart-angleEnd;
        if(angleTemp<0)angleTemp+=360;

        painter->rotate(360-angleStart+angleTemp*i/(numScaleMark-1)+90);
        painter->drawText(-offsetScaleTextRatio*bRect.width()/20, -offsetScaleTextRatio*bRect.width()/2, \
                          tr("%1%2%3").arg(markPrefix).arg(numStart+i*(numEnd-numStart)/(numScaleMark-1)).arg(markPostfix));
        painter->restore();
    }

    //Set pen style to draw title of gauge
    pen.setStyle(Qt::SolidLine);
    pen.setWidth(2);
    pen.setBrush(Qt::blue);
    painter->setPen(pen);
    font = painter->font();
    font.setPixelSize(24);
    painter->setFont(font);
    painter->drawText(-offsetTitleTextX*bRect.width()/2, offsetTitleTextY*bRect.width()/2, title);

    //Set pen & brush to draw pointer
    pen.setStyle(Qt::SolidLine);
    pen.setWidth(1);
    pen.setBrush(Qt::black);
    pen.setCapStyle(Qt::RoundCap);
    pen.setJoinStyle(Qt::RoundJoin);
    painter->setPen(pen);
    painter->save();
    angleTemp=angleStart-angleEnd;
    if(angleTemp<0)angleTemp+=360;
    painter->rotate(360-angleStart+angleTemp*(numCurrent-numStart)/(numEnd-numStart));
    painter->setBrush(Qt::red);
    polyPointer[0]=QPoint(0.35*bRect.width()+1, 0);
    polyPointer[1]=QPoint(0, -0.05*bRect.width()-1);
    polyPointer[2]=QPoint(-0.05*bRect.width()-1, 0);
    polyPointer[3]=QPoint(0, 0.05*bRect.width()+1);
    painter->drawPolygon(polyPointer,4);
    painter->restore();

    //Draw central pole
    pen.setStyle(Qt::SolidLine);
    pen.setWidth(2);
    pen.setBrush(Qt::black);
    painter->setBrush(Qt::blue);
    painter->setPen(pen);
    painter->drawEllipse(-bRect.width()/poleScale-1, -bRect.width()/poleScale-1, \
                         (bRect.width()/poleScale+1)*2, (bRect.width()/poleScale+1)*2);

}

//**************************************
// Set the rect of gauge (in local coordinates)
// The rect must be square type otherwise no effect.
// The gauge rect will be used for defining boundary & shape.
//**************************************
void mmGauge::setRect(QRect rect)
{
    if(rect.width() == rect.height())
        this->bRect = rect;
}

void mmGauge::setAngleStart(int num)
{
    angleStart=num;
}

void mmGauge::setAngleEnd(int num)
{
    angleEnd=num;
}

void mmGauge::setNumStart(float num)
{
    numStart=num;
}

void mmGauge::setNumEnd(float num)
{
    numEnd=num;
}

void mmGauge::setNumCurrent(float num)
{
    if(num>qMax(numStart, numEnd)){
        numCurrent=qMax(numStart, numEnd);
    }else if(num<qMin(numStart, numEnd)){
        numCurrent=qMin(numStart, numEnd);
    }else{
        numCurrent=num;
    }
}

void mmGauge::setNumScaleMark(int num)
{
    if(num>1)numScaleMark=num;
}

void mmGauge::setPoleScale(int num)
{
    poleScale=num;
}

void mmGauge::setTitle(QString tl)
{
    title=tl;
}

void mmGauge::setOffsetTitleTextX(float off)
{
    offsetTitleTextX = off;
}

void mmGauge::setOffsetTitleTextY(float off)
{
    offsetTitleTextY = off;
}

float mmGauge::getNumStart()
{
    return numStart;
}

float mmGauge::getNumCurrent()
{
    return numCurrent;
}

float mmGauge::getNumEnd()
{
    return numEnd;
}

void mmGauge::advance(int step)
{
    if(step == 0)return;
}

