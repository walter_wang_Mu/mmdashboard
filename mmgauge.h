#ifndef MMGAUGE_H
#define MMGAUGE_H

#include <QGraphicsItem>
#include <QRandomGenerator>

class mmGauge : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
    Q_PROPERTY(float numCurrent READ getNumCurrent WRITE setNumCurrent)

public:
    mmGauge();

    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget) override;

    void setRect(QRect rect);
    void setAngleStart(int num);
    void setAngleEnd(int num);
    void setNumStart(float num);
    void setNumEnd(float num);
    void setNumCurrent(float num);
    void setNumScaleMark(int num);
    void setPoleScale(int num);
    void setTitle(QString tl);
    void setOffsetTitleTextX(float off);
    void setOffsetTitleTextY(float off);
    float getNumStart();
    float getNumCurrent();
    float getNumEnd();

protected:
    void advance(int step) override;

private:
    QRect bRect = QRect(-50, -50, 100, 100);            //The size of boundary square occupied by gauge.
    QColor color = QColor(QRandomGenerator::global()->bounded(256),
                         QRandomGenerator::global()->bounded(256),
                         QRandomGenerator::global()->bounded(256));
    int angleStart = 225;                       //Start angle of gauge.
    int angleEnd = 315;                         //End angle of gauge.
    float numStart = 0;                           //Number of start point.
    float numEnd = 100;                           //Number of end point.
    float numCurrent = 0;                         //Current position of the dial pointer.
    int numScaleMark = 11;                      //Number of scale marks, default is 11, e.g. 10 scaled sections.
    int offsetScale = 10;                       //The min distance between the rect boundary and scale arc.
    int poleScale = 10;                         //Define the scale of Dial size v.s. dial pole. 10 means the pole size is 1/10 of dial size.
    float offsetScaleTextRatio = 0.7;           //
    float offsetTitleTextY = 0.5;               //
    float offsetTitleTextX = 0.2;               //

    QString markPrefix=tr("");                      //Prefix of each scale mark string.
    QString markPostfix=tr("");                     //Postfix of each scale mark string.
    QString title=tr("Gauge");                      //Title text of gauge.

    QPoint polyPointer[4]={
        QPoint(-10,-10),
        QPoint(50,0),
        QPoint(-10,10),
        QPoint(-10,0)
    };
};

#endif // MMGAUGE_H
